<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./dist/global.css">
    <title>Bitcoin Profit</title>
  </head>
  <body>
    <?php include './components/hero.php' ?>
    <?php include './components/videoForm.php' ?>
    <?php include './components/faq.php' ?>
    <?php include './components/testoms.php' ?>
    <?php include './components/signup.php' ?>
    <?php include './components/footer.php' ?>
    <script src="./dist/global.js"></script>
  </body>
</html>