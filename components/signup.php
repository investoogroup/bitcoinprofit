<section class="signup">
  <div class="container">
    <div class="signup__header-wrapper">
      <h2 class="signup__header">Sign Up Below</h2>
      <h3 class="signup__sub-header">and we’ll guide you through the process.</h3>
    </div>
    <div class="signup__form-wrapper">
      <?php include './components/formSmall.php' ?>
    </div>
  </div>
</section>