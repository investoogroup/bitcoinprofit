<section class="hero">
  <div class="container">
    <div class="hero__wrapper">
      <img class="hero__logo" src="./media/images/logo.svg">
      <h1 class="hero__header">
        <span class="hero__header--block">Earn millions from bitcoin</span>
        <span class="hero__header--block"> even when crypto markets</span>
        <span class="hero__header--block">are crashing</span>
      </h1>
      <h2 class="hero__sub-header">
        <a class="hero__cta" href="#">Join today</a>
        <span class="hero__sub-header--block"> and see</span>
        <span class="hero__sub-header--block"> how much you can make</span>
      </h2>
    </div>
  </div>
</section>