<section class="testoms">
  <div class="container">
    <div class="testoms__header-wrapper">
      <h2 class="testoms__header">What others have to say</h2>
      <h3 class="testoms__sub-header">We asked some people to give us their opinion on our app.</h3>
    </div>
    <div class="testoms__videos">
      <div class="testoms__video">
        <video controls width="100%">
          <source src="../media/videos/testoms/en-1-t1.mp4" type="video/mp4">
        </video>
      </div>
      <div class="testoms__video testoms__video--middle">
        <video controls width="100%">
          <source src="../media/videos/testoms/en-1-t2.mp4" type="video/mp4">
        </video>
      </div>
      <div class="testoms__video">
        <video controls width="100%">
          <source src="../media/videos/testoms/en-1-t3.mp4" type="video/mp4">
        </video>
      </div>
    </div>
    <div class="testoms__stories">
      <div class="testoms__stories-row">
        <div class="testoms__story-img-wrapper">
          <img class="testoms__story" src="./media/images/testoms/story-img-en-1.png">
        </div>
        <div class="testoms__story-img-wrapper">
          <img class="testoms__story" src="./media/images/testoms/story-img-en-2.png">
        </div>
      </div>
      <div class="testoms__stories-row">
        <div class="testoms__story-img-wrapper">
          <img class="testoms__story" src="./media/images/testoms/story-img-en-3.png">
        </div>
        <div class="testoms__story-img-wrapper">
          <img class="testoms__story" src="./media/images/testoms/story-img-en-4.png">
        </div>
      </div>
    </div>
  </div>
</section>


