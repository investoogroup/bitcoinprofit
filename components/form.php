<section class="form">
  <form>
    <div class="form__wrapper">
      <label class="form__label">First Name</label>
      <input class="form__input" type="text" placeholder="First Name">
      <label class="form__label">Email</label>
      <input class="form__input" type="text" placeholder="Email">
      <button class="form__submit-btn">GET FREE ACCESS!</button>
      <div class="form__disclaimers">
        <p class="form__disclaimer">*By submitting you confirm that you’ve read and accepted the privacy policy and terms of conditions.</p>
        <p class="form__disclaimer">**By submitting this form, I agree to receive all marketing material by email, SMS and telephone.</p>
      </div>
      <img class="form__badges" src="./media/images/form-badges.png">
    </div>
  </form>
</section>