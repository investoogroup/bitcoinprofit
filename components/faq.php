<section class="faq">
  <div class="container">
    <div class="faq__header-wrapper">
      <h2 class="faq__header">FAQ</h2>
      <h3 class="faq__sub-header">Here are the most common questions<br> with our answers.</h3>
    </div>
    <div class="faq__row">
      <div class="faq__question-wrapper">
        <div class="faq__number-wrapper">
          <div class="faq__number faq__number--primary">Q1</div>
          <div class="faq__number faq__number--secondary">A1</div>
        </div>
        <div class="faq__question-text">
          <h4 class="faq__question-header">Is Bitcoin safe?</h4>
          <p class="faq__question-answer">If you look at bitcoin’s history it has lots of ups and downs. However, look more closely and you’ll notice that after every time it goes down it increases to a new high. This is because Bitcoin is constantly growing.</p>
        </div>
      </div>
      <div class="faq__question-wrapper faq__question-wrapper--middle">
        <div class="faq__number-wrapper">
          <div class="faq__number faq__number--primary">Q2</div>
          <div class="faq__number faq__number--secondary">A2</div>
        </div>
        <div class="faq__question-text">
          <h4 class="faq__question-header">What’s in it for us?</h4>
          <p class="faq__question-answer">At Bitcoin Profit, we are all investors in Bitcoin. Therefore, we know that the more people that join us, the bigger Bitcoin will grow.</p>
        </div>
      </div>
      <div class="faq__question-wrapper">
        <div class="faq__number-wrapper">
          <div class="faq__number faq__number--primary">Q3</div>
          <div class="faq__number faq__number--secondary">A3</div>
        </div>
          <div class="faq__question-text">
            <h4 class="faq__question-header">What’s the secret?</h4>
            <p class="faq__question-answer">The secret is that there is no secret. Join today and we’ll show you just how easy it is to get started trading Bitcoin.</p>
          </div>
        </div>
      </div>
    <div class="faq__row">
      <div class="faq__question-wrapper">
        <div class="faq__number-wrapper">
          <div class="faq__number faq__number--primary">Q4</div>
          <div class="faq__number faq__number--secondary">A4</div>
        </div>
        <div class="faq__question-text">
          <h4 class="faq__question-header">Is it risky?</h4>
          <p class="faq__question-answer">The risk levels depend on how you trade. Join us today and we’ll show you how to maximise your profits and minimise your risk.</p>
        </div>
      </div>
      <div class="faq__question-wrapper faq__question-wrapper--middle">
        <div class="faq__number-wrapper">
          <div class="faq__number faq__number--primary">Q5</div>
          <div class="faq__number faq__number--secondary">A5</div>
        </div>
        <div class="faq__question-text">
          <h4 class="faq__question-header">Why Join Bitcoin Profit?</h4>
          <p class="faq__question-answer">Bitcoin Profit enables you to easily trade your Bitcoin for real cash. We also have a state of the art platform which makes trading easier than ever before. Don’t believe us? Join today and see for yourself. Getting involved in the Bitcoin revolution has never been easier.</p>
        </div>
      </div>
      <div class="faq__question-wrapper">
        <div class="faq__number-wrapper">
          <div class="faq__number faq__number--primary">Q6</div>
          <div class="faq__number faq__number--secondary">A6</div>
        </div>
        <div class="faq__question-text">
          <h4 class="faq__question-header">What’s the catch?</h4>
          <p class="faq__question-answer">No catch! You simply sign up, deposit an initial trading amount of 250 £ and we’ll do the rest. Our platform enables the easiest way to get started trading Bitcoin. No hassle, no fuss so that you can start earning instantly.</p>
          </div>
        </div>
    </div>
  </div>
</section>