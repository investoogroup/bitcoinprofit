<section class="videoForm">
  <div class="container">
    <div class="videoForm__wrapper">
      <div class="row">
        <div class="col-md-12 col-lg-7">
          <video controls autoplay width="100%">
            <source src="./media/videos/videoForm.mp4" type="video/mp4">
          </video>
        </div>
        <div class="col-md-12 col-lg-5">
          <div class="videoForm__form">
            <div class="videoForm__form-title">
              <span class="videoForm__form-title--block videoForm__form-title--highlighted">SIGN UP BELOW</span>
              <span class="videoForm__form-title--block"> and we’ll guide you through</span>
              <span class="videoForm__form-title--block">the process.</span>
            </div>
            <?php include './components/form.php' ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>