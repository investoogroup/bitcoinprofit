<section class="form-small">
  <form>
    <div class="form-small__wrapper">
      <label class="form-small__label">First Name</label>
      <input class="form-small__input" type="text" placeholder="First Name">
      <label class="form-small__label">Email</label>
      <input class="form-small__input" type="text" placeholder="Email">
      <button class="form-small__submit-btn">GET FREE ACCESS!</button>
      <div class="form-small__disclaimers">
        <p class="form-small__disclaimer">*By submitting you confirm that you’ve read and accepted the privacy policy and terms of conditions.</p>
        <p class="form-small__disclaimer">**By submitting this form, I agree to receive all marketing material by email, SMS and telephone.</p>
      </div>
    </div>
  </form>
</section>