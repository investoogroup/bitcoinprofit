const popups = document.querySelectorAll("[data-footer-popup]");
const links = document.querySelectorAll("[data-footer-link]");

const switchClass = (id, classAdd, classRemove) => {
  let add = id.classList.add(classAdd);
  let remove = id.classList.remove(classRemove);
  return { add, remove };
};

links.forEach(link => {
  link.addEventListener("click", () => {
    const linkPopup = link.nextElementSibling;
    const popupClose = document.getElementsByClassName("footer__close-btn");
    for (let i = 0; i < popupClose.length; i++) {
      popupClose[i].addEventListener("click", () => {
        switchClass(
          linkPopup,
          "footer__popup--hidden",
          "footer__popup--visisble"
        );
      });
    }
    switchClass(linkPopup, "footer__popup--visisble", "footer__popup--hidden");
  });
});
